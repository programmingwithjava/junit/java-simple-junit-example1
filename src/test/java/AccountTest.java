import org.junit.*;

import static org.junit.Assert.*;

public class AccountTest {

    private Account account;
    private static int count;

    /*
    * org.junit.Before annotation telling JUnit framework to run this "setup" method every time we run a test.
    * before any of the test methods below is executed, this setup method is executed to create a brand new
    * Account object. Otherwise we would have to instantiate a new object in each and every one of the test methods below.
    * */
    @Before
    public void beforeTest() {
        account = new Account("serdar", "onur", 1000.00, Account.CHECKING);
        System.out.println("running test method...");
    }

    @After
    public void afterTest() {
        System.out.println("count = " + count++);
    }

    /*
    @BeforeClass annotated methods will run once per run, not once per method invocation. Like opening a socket or
    reading a file.

    @AfterClass annotated methods work the same way. They run after all the test methods have been run.
    We can use @AfterClass annotated methods to run cleanup code.

    @BeforeClass and @AfterClass annotated methods should be static.
    * */
    @BeforeClass
    public static void beforeClass() {
        System.out.println("setup before executing any tests. count = " + count++);
    }

    @AfterClass
    public static void afterClass() {
        System.out.println("cleanup after executing all the tests. count = " + count++);
    }

    @org.junit.Test
    public void deposit() throws Exception {
        double balance = account.deposit(100.00, true);
        assertEquals(1100.00, balance, .01);
    }

    @org.junit.Test
    public void withdraw_morethan_500_fromBranch() {
        double balance = account.withdraw(501.00, true);
        assertEquals(499.00, balance, .01);
    }

    /*
    * In this test we are actually expecting to get an IllegalArgumentException.
    * We specify this using the "expected" Annotation Element. So that our test will pass as expected.
    * Otherwise the test would fail with an IllegalArgumentException. Since we expect to get this exception,
    * our test should actually pass. assertEquals test is not required in this case,
    * */
    @Test(expected = IllegalArgumentException.class)
    public void withdraw_morethan_500_fromATM() {
        account.withdraw(501.00, false);

        /*
        * if an IllegalArgumentException is not thrown, then we will fail the test.
        * */
        fail("should have thrown IllegalArgumentException");
    }

    /*
    * Annotations were introduced in JUnit 4. Before that, the expected exception would be caught in a
    * try-catch block and we wouldn't do anything in the catch block.
    * We would pass the test by catching the exception.
    * */
    @Test
    public void withdraw_morethan_500_fromATM_oldWay() {
        try {
            account.withdraw(501.00, false);
            /*
            * if an IllegalArgumentException is not thrown, then we will fail the test.
            * */
            fail("should have thrown IllegalArgumentException");
        } catch (IllegalArgumentException ex) {

        }

    }

    @org.junit.Test
    public void getBalance_afterDeposit() {
        double balance = account.deposit(100.00, true);
        assertEquals(1100.00, account.getBalance(), .01);
    }

    @org.junit.Test
    public void getBalance_afterWithdraw() {
        double balance = account.withdraw(100.00, true);
        assertEquals(900.00, account.getBalance(), .01);
    }

    @org.junit.Test
    public void isChecking_true() {
        assertTrue(account.isChecking());
    }

    /*
    * empty test methods will look to be successful. you can fail a test to be implemented
    * later as below so that you won't miss them.
    * Note: If you are failing your CI/CD pipelines with failing unit test you may not like to do so.
    * */
    @Test
    public void toBeImplemented() {
        fail("this test will be implemented later");
    }

    /*
    * print statement are spooled in an IO thread. so the print out from the above methods might not always
    * come in the order that we expect. but the methods always execute in the order dictated by the
    * annotations Before, After, BeforeClass, AfterClass.
    * */
}